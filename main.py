#!/usr/bin/env python3
import sys
import pygame
import math

def speed_from_angle(angle, factor):
  """Restituisce le velocita' x ed y per muovere
  un oggetto secondo l'angolo specificato.
  Argomenti:
    angle  - angolo in gradi (0 - 360)
    factor - fattore di rapidità (minimo 5 per
              sufficiente precisione)
  Restituisce:
    [vel_x, vel_y] - lista
  """
  rad = 0 - math.radians(angle)
  return [
    factor * math.cos(rad),
    factor * math.sin(rad)
  ]

pygame.init()
clock = pygame.time.Clock()
screen = pygame.display.set_mode((500, 500))
angle = 0

while True:

    freccia = pygame.image.load("freccia.png")
    freccia = pygame.transform.rotate(freccia, angle)
    vel_x, vel_y = speed_from_angle(angle, 7)
    freccia_rect = freccia.get_rect()
    freccia_rect.x = 250
    freccia_rect.y = 250

    while 0 < freccia_rect.x < 500 and 0 < freccia_rect.y < 500:
      # finche' la freccia sta dentro lo schermo

      for event in pygame.event.get():
          if event.type == pygame.QUIT: sys.exit()

      freccia_rect.x += vel_x
      freccia_rect.y += vel_y

      screen.fill((0,0,0))
      screen.blit(freccia, freccia_rect)
      pygame.display.update()
      clock.tick(60)
    
    angle = (angle + 15) % 360
    # incremento circolare di angle
    # da 0 a 360 (escluso) con passo di 15
    # cioe': 0, 15, 30, ...,  345, 0, 15, 30, ...
